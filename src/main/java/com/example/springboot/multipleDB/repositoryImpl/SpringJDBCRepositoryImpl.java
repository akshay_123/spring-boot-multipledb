package com.example.springboot.multipleDB.repositoryImpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SpringJDBCRepositoryImpl {
	@Autowired
	@Qualifier("userJdbcTemplate")
	private JdbcTemplate userJdbcTemplate;
	
	@Autowired
	@Qualifier("bookJdbcTemplate")
	private JdbcTemplate bookJdbcTemplate;
	
	@Transactional
	public Object getAllUsers() {
		return this.userJdbcTemplate.queryForList("select * from user");
	}
	
	@Transactional
	public Object getAllBooks() {
		return this.bookJdbcTemplate.queryForList("select * from book");
	}
	
	@Transactional
	public Object getAllJDBCUsers() {
		return this.userJdbcTemplate.queryForList("select * from jdbc_user");
	}

}
