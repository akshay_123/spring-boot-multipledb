package com.example.springboot.multipleDB.repositoryImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.example.springboot.multipleDB.entity.user.User;
import com.example.springboot.multipleDB.repository.UserCustomRepository;

@Repository(value = "UserCustomRepository")
public class UserCustomRepositoryImpl implements UserCustomRepository {

	@Autowired
	//if we have multiple Db then fetch rewuired one.
	@Qualifier("userEntityManagerFactory")
	EntityManager entityManager;
	@Override
	@Transactional
	public List<User> getAlluserFromCustomRepository() {
		// TODO Auto-generated method stub
		String hql="SELECT u from User u";
		return this.entityManager.createQuery(hql).getResultList();
		
		//return null;
	}

}
