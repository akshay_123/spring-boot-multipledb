package com.example.springboot.multipleDB.config;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef = "bookEntityManagerFactory", 
		transactionManagerRef = "bookTransactionManager",
		basePackages = {"com.example.springboot.multipleDB.repository.book"}
		)
public class BookDBConfiguration {
	
		@Bean(name = "bookDatasource")
		//we need to mention db for which need to create db.please add same prefix by referring d property
		@ConfigurationProperties(prefix = "spring.book.datasource")
		public DataSource datasource() {
			return DataSourceBuilder.create().build();
		}
		
		@Bean(name = "bookJdbcTemplate")
		 public JdbcTemplate jdbcTemplate1(@Qualifier("bookDatasource") DataSource ds) {
		  return new JdbcTemplate(ds);
		 }
		
		//Configure Entity manager for our datasource
		@Bean(name = "bookEntityManagerFactory")
		public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
				@Qualifier("bookDatasource") DataSource dataSource) {
			HashMap<String, Object> properties = new HashMap<>();
			properties.put("hibernate.hbm2ddl.auto", "update");
			properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
			return builder.dataSource(dataSource).properties(properties)
					.packages("com.example.springboot.multipleDB.entity.book").persistenceUnit("Book").build();
		}
		
		
		//configure transaction manager
		@Bean(name = "bookTransactionManager")
		public PlatformTransactionManager transactionManager(
				@Qualifier("bookEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
			return new JpaTransactionManager(entityManagerFactory);
		}

	
}