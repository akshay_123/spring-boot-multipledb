package com.example.springboot.multipleDB.repository.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.multipleDB.entity.book.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
