package com.example.springboot.multipleDB.repository;

import java.util.List;

import com.example.springboot.multipleDB.entity.user.User;

public interface UserCustomRepository {
	List<User> getAlluserFromCustomRepository();

}
