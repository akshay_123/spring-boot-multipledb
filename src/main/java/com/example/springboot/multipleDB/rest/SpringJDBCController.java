package com.example.springboot.multipleDB.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.multipleDB.repositoryImpl.SpringJDBCRepositoryImpl;

@RestController
@RequestMapping("/jdbc")
public class SpringJDBCController {
	@Autowired
	SpringJDBCRepositoryImpl springJDBCRepositoryImpl;
	
	@GetMapping("/users")
	public Object getAllUserUsingJDBCTemplate() {
		return this.springJDBCRepositoryImpl.getAllUsers();
	}

	@GetMapping("/books")
	public Object getAllBooksUsingJDBCTemplate() {
		return this.springJDBCRepositoryImpl.getAllBooks();
	}

	@GetMapping("/jdbc_users")
	public Object getAllJDBCUsers() {
		return this.springJDBCRepositoryImpl.getAllJDBCUsers();
	}

}
