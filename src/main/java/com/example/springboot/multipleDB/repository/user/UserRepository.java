package com.example.springboot.multipleDB.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.multipleDB.entity.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
