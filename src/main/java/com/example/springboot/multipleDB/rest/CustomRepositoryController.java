package com.example.springboot.multipleDB.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.multipleDB.entity.user.User;
import com.example.springboot.multipleDB.repository.UserCustomRepository;

@RestController
public class CustomRepositoryController {
	@Autowired
	private UserCustomRepository userCustomRepository;
	@GetMapping("/users")
	public List<User> getAlluserFromCustomRepository(){
		return this.userCustomRepository.getAlluserFromCustomRepository();
	}

}
