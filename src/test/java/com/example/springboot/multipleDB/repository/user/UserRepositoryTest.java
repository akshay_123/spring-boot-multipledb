package com.example.springboot.multipleDB.repository.user;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.springboot.multipleDB.entity.user.User;

@SpringBootTest
class UserRepositoryTest {

	@Autowired
	private UserRepository UserRepository;
	

//	@Test
//	void saveUsers() {
//		List<User> userList=IntStream.range(0, 10)
//				.mapToObj(i->User.builder().name("User"+i).email("user"+i+"@gmail.com").build())
//				.collect(Collectors.toList());
//		this.UserRepository.saveAll(userList);
//	}
	
	@Test
	public void getAllUsers() {
		System.out.println(this.UserRepository.findAll());
	}

}
